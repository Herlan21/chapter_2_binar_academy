import React from 'react';
import { StyleSheet, View, Image, Dimensions,Text} from "react-native";


const card = () => {
      return (
        <View style={styles.container}>
            <View style={styles.card}>
                
                <Image
                style={styles.image}
                resizeMode="cover"
                source={{
                    uri:"https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png",
                }}
                
                />
            </View>


                <View>
                        <Text 
                        style={{
                            marginTop: 10, 
                            color: "black",
                            fontWeight: "bold"
        
                        }}>Styling di React Native
                        </Text>
                </View>

                <View>
                    <Text 
                    style={{marginTop: 3}}>Binar Academy - React Native   
                    </Text>
                </View>

                <View>
                    <Text
                    style={{
                        marginTop: 5,
                        width: 330,
                        fontWeight: "bold"
                    }}>As a compund component grows in complexity, it is much clearer and efficient to use StyleSheet.Create so as to define several styles in one place.
                    </Text>
                </View>
            
        </View>
      );
    }

    
    const screen = Dimensions.get("screen");
    const styles = StyleSheet.create({

        container: {
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            marginTop: 45
        },

        card: {
            backgroundColor: "#eee",
            width: screen.width * 0.9,
            
        },

        image: {
            height: screen.width * 0.8,
            borderRadius: 10,
            
        },

        
        nameText: {
            fontWeight: "bold",
            color: "black",
            alignItems:"flex-end"
            
        },

    });


export default card;
