import React from 'react';
import { StyleSheet, View, Image, Dimensions} from "react-native";
import{
    Text,
} from 'react-native'

export default function App(){
      return (
        <View style={styles.container}>
            <View style={styles.card}>
                <View>
                    <Text style={styles.nameText}>React Native School</Text>
                    <Text style={styles.followText}>Follow</Text>
                </View>
                <Image
                style={styles.image}
                resizeMode="cover"
                source={{
                    uri:"https://images.pexels.com/photos/3225517/pexels-photo-3225517.jpeg?cs=srgb&dl=pexels-michael-block-3225517.jpg",
                }}
                />
                <View>
                    <Text>
                        <Text style={styles.nameText}>React Native School</Text>
                        Dalam praktiknya penggunaan function/fungsi merupakan hal yang sangat membantu dalam proses pembuatan sistem, karena dengan menggunakan fungsi kita dapat menyingkat penggunaan kode yang harus digunakan secara berulang dan digantikan dengan fungsi yang telah dibuat sebelumnya.
                    </Text>
                </View>
            </View>
        </View>
      );
    }

    
    const screen = Dimensions.get("screen");
    const styles = StyleSheet.create({

        container: {
            flex: 1,
            backgroundColor: "#7CA184",
            alignItems: "center",
            justifyContent: "center",
        },

        card: {
            backgroundColor: "#fff",
            width: screen.width * 0.8,
        },

        image: {
            height: screen.width * 0.8,
        },

        nameText: {
            fontWeight: "bold",
        },
        
        nameText: {
            fontWeight: "bold",
            color: "#20232a",
        },

        followText: {
            fontWeight: "bold",
            color: "#0095f6",
        },
    });



